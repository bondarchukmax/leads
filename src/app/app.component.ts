import {Component, OnInit} from '@angular/core';

declare const $: any;

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})


export class AppComponent implements OnInit{
    shopList = [
        {
            'id': 1,
            'name': 'Видеокамера SJCAM SJ4000 WiFi',
            'img': 'http://avatars.mds.yandex.net/get-mpic/199079/img_id8067599458589738077/6hq',
            'price': '3 145 ₽',
            'raiting': 4.1,
            'description': ['экшн-камера', 'запись видео UHD 4K на карты памяти',
                'матрица 8.2 МП (1/2.5")', 'карты памяти MS, microSD, microSDHC', 'до 2.35 ч работы от аккумулятора']
        },
        {
            'id': 2,
            'name': 'Видеокамера SJCAM SJ4000',
            'img': 'http://avatars.mds.yandex.net/get-mpic/175985/img_id567282225509542107/6hq',
            'price': '3 245 ₽',
            'raiting': 4.2,
            'description': ['экшн-камера', 'запись видео UHD 4K на карты памяти',
                'матрица 8.2 МП (1/2.5")', 'карты памяти MS, microSD, microSDHC', 'до 2.35 ч работы от аккумулятора']
        },
        {
            'id': 3,
            'name': 'Видеокамера YI Action Camera Basic Edition',
            'img': 'http://avatars.mds.yandex.net/get-mpic/175985/img_id6763452806388156299/6hq',
            'price': '3 345 ₽',
            'raiting': 4.4,
            'description': ['экшн-камера', 'запись видео UHD 4K на карты памяти',
                'матрица 8.2 МП (1/2.5")', 'карты памяти MS, microSD, microSDHC', 'до 2.35 ч работы от аккумулятора']
        },
        {
            'id': 4,
            'name': 'Видеокамера Sony FDR-X3000',
            'img': 'http://avatars.mds.yandex.net/get-mpic/195452/img_id5056237481297111766/6hq',
            'price': '3 445 ₽',
            'raiting': 4.5,
            'description': ['экшн-камера', 'запись видео UHD 4K на карты памяти',
                'матрица 8.2 МП (1/2.5")', 'карты памяти MS, microSD, microSDHC', 'до 2.35 ч работы от аккумулятора']
        },
        {
            'id': 5,
            'name': 'Видеокамера Sony FDR-X3000R',
            'img': 'http://avatars.mds.yandex.net/get-mpic/175985/img_id5598280740873343326/6hq',
            'price': '3 545 ₽',
            'raiting': 5.0,
            'description': ['экшн-камера', 'запись видео UHD 4K на карты памяти',
                'матрица 8.2 МП (1/2.5")', 'карты памяти MS, microSD, microSDHC', 'до 2.35 ч работы от аккумулятора']
        },
        {
            'id': 6,
            'name': 'Видеокамера Sony HDR-AS50',
            'img': 'http://avatars.mds.yandex.net/get-mpic/200316/img_id470669386132914325/6hq',
            'price': '3 645 ₽',
            'raiting': 4.5,
            'description': ['экшн-камера', 'запись видео UHD 4K на карты памяти',
                'матрица 8.2 МП (1/2.5")', 'карты памяти MS, microSD, microSDHC', 'до 2.35 ч работы от аккумулятора']
        },
        {
            'id': 7,
            'name': 'Видеокамера YI 4K Action Camera',
            'img': 'http://avatars.mds.yandex.net/get-mpic/199079/img_id8067599458589738077/6hq',
            'price': '3 745 ₽',
            'raiting': 4.5,
            'description': ['экшн-камера', 'запись видео UHD 4K на карты памяти',
                'матрица 8.2 МП (1/2.5")', 'карты памяти MS, microSD, microSDHC', 'до 2.35 ч работы от аккумулятора']
        },
        {
            'id': 8,
            'name': 'Видеокамера YI 4K Action Camera',
            'img': 'http://avatars.mds.yandex.net/get-mpic/96484/img_id3536813077196049508/6hq',
            'price': '3 845 ₽',
            'raiting': 4.5,
            'description': ['экшн-камера', 'запись видео UHD 4K на карты памяти',
                'матрица 8.2 МП (1/2.5")', 'карты памяти MS, microSD, microSDHC', 'до 2.35 ч работы от аккумулятора']
        },
        {
            'id': 9,
            'name': 'Видеокамера Sony FDR-X3000R',
            'img': 'http://avatars.mds.yandex.net/get-mpic/199079/img_id8067599458589738077/6hq',
            'price': '3 915 ₽',
            'raiting': 4.5,
            'description': ['экшн-камера', 'запись видео UHD 4K на карты памяти',
                'матрица 8.2 МП (1/2.5")', 'карты памяти MS, microSD, microSDHC', 'до 2.35 ч работы от аккумулятора']
        },
        {
            'id': 10,
            'name': 'Видеокамера Sony FDR-X3000R',
            'img': 'http://avatars.mds.yandex.net/get-mpic/199079/img_id8067599458589738077/6hq',
            'price': '3 925 ₽',
            'raiting': 4.5,
            'description': ['экшн-камера', 'запись видео UHD 4K на карты памяти',
                'матрица 8.2 МП (1/2.5")', 'карты памяти MS, microSD, microSDHC', 'до 2.35 ч работы от аккумулятора']
        },
        {
            'id': 11,
            'name': 'Видеокамера Sony FDR-X3000R',
            'img': 'http://avatars.mds.yandex.net/get-mpic/199079/img_id4632456463507925745/6hq',
            'price': '3 935 ₽',
            'raiting': 4.5,
            'description': ['экшн-камера', 'запись видео UHD 4K на карты памяти',
                'матрица 8.2 МП (1/2.5")', 'карты памяти MS, microSD, microSDHC', 'до 2.35 ч работы от аккумулятора']
        },
        {
            'id': 12,
            'name': 'Видеокамера Sony FDR-X3000R',
            'img': 'http://avatars.mds.yandex.net/get-mpic/199079/img_id8067599458589738077/6hq',
            'price':     '3 955 ₽',
            'raiting': 4.5,
            'description': ['экшн-камера', 'запись видео UHD 4K на карты памяти',
                'матрица 8.2 МП (1/2.5")', 'карты памяти MS, microSD, microSDHC', 'до 2.35 ч работы от аккумулятора']
        },
        {
            'id': 13,
            'name': 'Видеокамера Sony FDR-X3000R',
            'img': 'http://avatars.mds.yandex.net/get-mpic/199079/img_id8067599458589738077/6hq',
            'price': '3 965 ₽',
            'raiting': 4.5,
            'description': ['экшн-камера', 'запись видео UHD 4K на карты памяти',
                'матрица 8.2 МП (1/2.5")', 'карты памяти MS, microSD, microSDHC', 'до 2.35 ч работы от аккумулятора']
        },
        {
            'id': 14,
            'name': 'Видеокамера Sony FDR-X3000R',
            'img': 'http://avatars.mds.yandex.net/get-mpic/199079/img_id8067599458589738077/6hq',
            'price': '3 975 ₽',
            'raiting': 4.5,
            'description': ['экшн-камера', 'запись видео UHD 4K на карты памяти',
                'матрица 8.2 МП (1/2.5")', 'карты памяти MS, microSD, microSDHC', 'до 2.35 ч работы от аккумулятора']
        },
        {
            'id': 15,
            'name': 'Видеокамера Sony FDR-X3000R',
            'img': 'http://avatars.mds.yandex.net/get-mpic/199079/img_id8067599458589738077/6hq',
            'price': '3 985 ₽',
            'raiting': 4.5,
            'description': ['экшн-камера', 'запись видео UHD 4K на карты памяти',
                'матрица 8.2 МП (1/2.5")', 'карты памяти MS, microSD, microSDHC', 'до 2.35 ч работы от аккумулятора']
        },
        {
            'id': 16,
            'name': 'Видеокамера Sony FDR-X3000R',
            'img': 'http://avatars.mds.yandex.net/get-mpic/199079/img_id4632456463507925745/6hq',
            'price': '3 995 ₽',
            'raiting': 4.5,
            'description': ['экшн-камера', 'запись видео UHD 4K на карты памяти',
                'матрица 8.2 МП (1/2.5")', 'карты памяти MS, microSD, microSDHC', 'до 2.35 ч работы от аккумулятора']
        },
        {
            'id': 17,
            'name': 'Видеокамера Sony FDR-X3000R',
            'img': 'http://avatars.mds.yandex.net/get-mpic/199079/img_id8067599458589738077/6hq',
            'price': ' 3 911 ₽',
            'raiting': 4.5,
            'description': ['экшн-камера', 'запись видео UHD 4K на карты памяти',
                'матрица 8.2 МП (1/2.5")', 'карты памяти MS, microSD, microSDHC', 'до 2.35 ч работы от аккумулятора']
        },
        {
            'id': 18,
            'name': 'Видеокамера Sony FDR-X3000R',
            'img': 'http://avatars.mds.yandex.net/get-mpic/199079/img_id8067599458589738077/6hq',
            'price': '3 922 ₽',
            'raiting': 4.5,
            'description': ['экшн-камера', 'запись видео UHD 4K на карты памяти',
                'матрица 8.2 МП (1/2.5")', 'карты памяти MS, microSD, microSDHC', 'до 2.35 ч работы от аккумулятора']
        },
        {
            'id': 19,
            'name': 'Видеокамера Sony FDR-X3000R',
            'img': 'http://avatars.mds.yandex.net/get-mpic/199079/img_id8067599458589738077/6hq',
            'price': '3 933 ₽',
            'raiting': 4.5,
            'description': ['экшн-камера', 'запись видео UHD 4K на карты памяти',
                'матрица 8.2 МП (1/2.5")', 'карты памяти MS, microSD, microSDHC', 'до 2.35 ч работы от аккумулятора']
        },
        {
            'id': 20,
            'name': 'Видеокамера Sony FDR-X3000R',
            'img': 'http://avatars.mds.yandex.net/get-mpic/199079/img_id4632456463507925745/6hq',
            'price': '3 944 ₽',
            'raiting': 4.5,
            'description': ['экшн-камера', 'запись видео UHD 4K на карты памяти',
                'матрица 8.2 МП (1/2.5")', 'карты памяти MS, microSD, microSDHC', 'до 2.35 ч работы от аккумулятора']
        }

    ];
    item: any[];
    item_img: string;
    item_rating: number;
    item_name: string;
    item_price: string;
    description: any[];
    cart: any ;
    savedList: any[];
    total: any;


    ngOnInit(){
        try {
            this.cart = JSON.parse(sessionStorage.getItem('shop_card')).length;
        }
        catch (e){
            this.cart = 0;
        }

    }
    getItem(id){
        this.item =  this.shopList.filter(function (item) {
            return item.id === id
        });
        this.item_name = this.item[0].name;
        this.item_img = this.item[0].img;
        this.item_price = this.item[0].price;
        this.item_rating = this.item[0].raiting;
        this.description = this.item[0].description;

    }
    putShop() {
        const listCart = sessionStorage.getItem('shop_card');
        const list = [];
        const savedList = JSON.parse(sessionStorage.getItem('shop_card')) || [];
        for (const saveditem of savedList ) {
            list.push(saveditem);
        }
        for (const item of this.item ) {
            list.push(item);
        }
        sessionStorage.setItem('shop_card', JSON.stringify(list));

        $('#item').modal('hide');
        this.cart += 1;

    }
    getSavedList(){
        this.savedList = JSON.parse(sessionStorage.getItem('shop_card'));
        function  totalPrice () {
            let totalPrice = 0;
            try {
                for ( const sv of JSON.parse(sessionStorage.getItem('shop_card'))){
                    const num = Number(sv.price.substring(0, sv.price.length - 2).replace(/\s/g, ''))
                    totalPrice += num;
                }
            }
            catch (e){
                let totalPrice = 0;
            }
            return totalPrice;
        }
        this.total = totalPrice();

        $('#shop_list').modal('show');
    }
    getIssue(){
        $('#shop_list').modal('hide');
        $('#issue').modal('show');
    }
    order(){
        $('#issue').modal('hide');
        $('#thanks').modal('show');
        this.cart = 0;
        sessionStorage.clear();
    }

    filterName(){
        this.shopList.sort( function(name1, name2) {
            if ( name1.name < name2.name ){
                return -1;
            }else if( name1.name > name2.name ){
                return 1;
            }else{
                return 0;
            }
        });
    }
    filterPrice(){
        this.shopList.sort( function(name1, name2) {
            if ( name1.price < name2.price ){
                return -1;
            }else if( name1.price > name2.price ){
                return 1;
            }else{
                return 0;
            }
        });
    }
    filterRating(){
        this.shopList.sort( function(name1, name2) {
            if ( name1.raiting < name2.raiting ){
                return -1;
            }else if( name1.raiting > name2.raiting ){
                return 1;
            }else{
                return 0;
            }
        });
    }
}
